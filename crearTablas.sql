CREATE TABLE `mainlogin` (
`id` int(11) NOT NULL,
`username` varchar(15) CHARACTER SET latin1 NOT NULL,
`email` varchar(40) CHARACTER SET latin1 NOT NULL,
`password` varchar(20) CHARACTER SET latin1 NOT NULL,
`role` varchar(10) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

INSERT INTO `mainlogin` (`id`, `username`, `email`, `password`, `role`) VALUES
(11, 'admin', 'demo@upv.com', '123456', 'admin'),
(12, 'test', 'test@upv.com', '123456', 'personal');

ALTER TABLE `mainlogin`
ADD PRIMARY KEY (`id`);
ALTER TABLE `mainlogin`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

