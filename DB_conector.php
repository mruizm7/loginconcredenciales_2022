<?php
// $db => NOMBRE DE LA VARIABLE DE CONEXIÓN A LA DATABASE

// DEFINICIÓN DE VARIABLES CON DATOS DE LA CONFIGURACIÓN DE LA DATABASE 

$db_host="localhost"      //dirección ip o nombre de dominio del servidor de base de datos  
$db_user="felipillo"      // Usuarios que accesará a la base de datos 
$db_password="1234567"  // Clave de acceso del usuario $db_user   
$db_name="marzo04"      //Nombre de la base de datos a la que se va a acceder. 

try
{
	$db=PDO("mysql:host={$db_host};dbname={$db_name}", $db_user, $db_password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOEXCEPTION $e) 
{
	$e->getMessage(); 
}   
?>
