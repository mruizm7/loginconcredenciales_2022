<div class="login-form"> 
<center><h2>Registrar</h2></center>
<form method="post" class="form-horizontal">

<div class="form-group">
<label class="col-sm-9 text-left">Usuario</label>
<div class="col-sm-12">
<input type="text" name="txt_username" class="form-control" placeholder="Ingrese usuario" />
</div>
</div>

<div class="form-group">
<label class="col-sm-9 text-left">Email</label>
<div class="col-sm-12">
<input type="text" name="txt_email" class="form-control" placeholder="Ingrese email" />
</div>
</div>

<div class="form-group">
<label class="col-sm-9 text-left">Password</label>
<div class="col-sm-12">
<input type="password" name="txt_password" class="form-control" placeholder="Ingrese password" />
</div>
</div>

<div class="form-group">
<label class="col-sm-9 text-left">Seleccione tipo</label>
<div class="col-sm-12">
<select class="form-control" name="txt_role">
<option value="" selected="selected"> - seleccione rol - </option>
<!--<option value="admin">Admin</option>-->
<option value="personal">Personal</option>
<option value="usuarios">Usuarios</option>
</select>
</div>
</div>

<div class="form-group">
<div class="col-sm-12">
<input type="submit" name="btn_register" class="btn btn-primary btn-block" value="Registro">
<!--<a href="index.php" class="btn btn-danger">Cancel</a>-->
</div>
</div>

<div class="form-group">
<div class="col-sm-12">
¿Tienes una cuenta? <a href="index.php"><p class="text-info">Inicio de sesión</p></a> 
</div>
</div>

</form>
</div><!--Cierra div login-->

