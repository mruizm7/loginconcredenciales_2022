
<DIV class="login-form">   
    <CENTER><H2>Iniciar sesión</H2></CENTER>
    <FORM method="post" class="form-horizontal">
         <DIV class="form-group">
           <LABEL class="col-sm-6 text-left">Email</LABEL>
              <DIV class="col-sm-12">
                 <INPUT type="text" name="txt_email" class="form-control" placeholder="Ingrese email" />
              </DIV>
         </DIV>
         <DIV class="form-group">
           <LABEL class="col-sm-6 text-left">Password</LABEL>
              <DIV class="col-sm-12">
                 <INPUT type="text" name="txt_password" class="form-control" placeholder="Ingrese el password" />
              </DIV>
         </DIV>
         
         <DIV class="form-group">
           <LABEL class="col-sm-6 text-left">Seleccionar el Rol</LABEL>
              <DIV class="col-sm-12">
                 <SELECT class="form-control" name="txt_role">
                    <OPTION value="" selected="selected">-seleccionar el rol-</option>
                    <OPTION value="admin">Admin</OPTION>
                    <OPTION value="personal">Personal</OPTION>
                    <OPTION value="usuarios">Usuarios</OPTION>
                 </SELECT>
              </DIV>
         </DIV>

         <DIV class="form-group"> 
             <DIV class="col-sm-12">
                <INPUT type="submit" name="btn_login" class="btn btn-success btn-block" value="Iniciar Sesion" />
             </DIV>
         </DIV>

         <DIV class="form-group">
             <DIV class="col-sm-12"> 
                  ¿No tiene una cuenta? <a href="registro.php"><p class="text-info">Registrar cuenta</p></a>
             </DIV>
          </DIV>

      </FORM>
   </DIV>





